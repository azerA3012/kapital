import React, { useState } from "react";
import "./main.css";
import "./css-fontawesome/all.css";
import Navi from "./components/Navi";
import Menu from "./components/Menu";
import View from "./components/View";

function App() {
  const [search, setSearch] = useState(false);
  const [load, setLoad] = useState(false);

  const [filial,setFilial]=useState(false)
  return (
    <div className="app">
      <div className="header">
        <Navi
          load={load}
          setLoad={setLoad}
          search={search}
          setSearch={setSearch}
          filial={filial}
          setFilial={setFilial}
        />
      </div>
      <div className="section">
        <Menu
          search={search}
          setSearch={setSearch}
          load={load}
          setLoad={setLoad}
          filial={filial}
          setFilial={setFilial}
        />
        <View
          search={search}
          setSearch={setSearch}
          load={load}
          setLoad={setLoad}
          filial={filial}
          setFilial={setFilial}
        />
      </div>
    </div>
  );
}

export default App;
