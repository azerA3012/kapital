import React from "react";

const Load = () => {
  return (
    <div className="load">
      <h4 className="load-file">load file</h4>
      <p className="process">process information</p>
      <div className="create-load-file">
          <div className="row">
            <div className="col-lg-3">
                <p className="main-page">Main page</p>
            </div>
            <div className="col-lg-9">
                <p className="history">history</p>
            </div>
            <div className="col-lg-3">
                <p className="initiator">initiator</p>
            </div>
            <div className="col-lg-3">
                <p className="user">11941 RehimliElcin</p>
            </div>
            <div className="col-lg-6">
                <div className="form-group row ml-0 mr-0">
                    <label  className="col-sm-2 col-form-label">Main</label>
                    <div className="col-sm-10 d-flex">
                        <input type="email" className="form-control"  />

                        <button className="btn btn-danger"><i className="fas fa-plus"></i></button>
                        <button className="btn btn-info"><i className="fas fa-search"></i></button>
                        <button className="btn btn-primary"><i className="fas fa-chevron-down"></i></button>
                    </div>
                </div>
            </div>
            <div className="col-lg-3">
                <p>File</p>
            </div>
            <div className="col-lg-3">
                <div className="upload-file">
                    <a href="#" className="upload-link">Upload file</a>
                    <i className="fas fa-camera"></i>
                </div>
            </div>
            <div className="col-lg-6">
                <div className="form-group row ml-0 mr-0">
                    <label  className="col-sm-2 col-form-label">Folder</label>
                    <div className="col-sm-10 d-flex">
                        <input type="email" className="form-control"  />

                        
                        <button className="btn btn-info"><i className="fas fa-search"></i></button>
                        <button className="btn btn-primary"><i className="fas fa-chevron-down"></i></button>
                    </div>
                </div>
            </div>
            <div className="col-lg-3"></div>
            <div className="col-lg-3"></div>
            <div className="col-lg-6">
                <div className="form-group row ml-0 mr-0">
                    <label  className="col-sm-2 col-form-label">Template</label>
                    <div className="col-sm-10 d-flex">
                        <input type="email" className="form-control"  />
                        <button className="btn btn-info"><i className="fas fa-search"></i></button>
                        <button className="btn btn-primary"><i className="fas fa-chevron-down"></i></button>
                    </div>
                </div>
            </div>
            

          </div>

      </div>
    </div>
  );
};

export default Load;
