import React from 'react'

const Filial = () => {
    return ( 
        <div className="filial">
            <ul className="filial-list">
                <li className="filial-item">
                    <img src="folder-svgrepo-com.svg" alt=""/>
                    <p>100</p>
                </li>
            </ul>
        </div>
     );
}
 
export default Filial;