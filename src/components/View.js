import React from 'react'
import Search from './Search'
import Load from './Load'
import Filial from './Filial'

const View = (props) => {
    return ( 
        <div className="view">
            {
                props.search?<Search/>:null
            }
            {
                props.load?<Load/>:null
            }
            {
                props.filial?<Filial/>:null
            }
            
            
            
            
            
        </div>
     );
}
 
export default View;