import React from 'react'

const Logo = () => {
    return ( 
        <div className="logo">
            <div className="logo-box">
                <img src="kapital-logo.svg" alt="" className="logo-img"/>
            </div>
            
            <div className="date">
                <h1 className="day">31 iyul</h1>
                <span className="week">friday</span>
            </div>
        </div>
     );
}
 
export default Logo;