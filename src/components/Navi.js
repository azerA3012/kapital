import React from "react";
import Logo from "./Logo";
import AddNew from "./AddNew";
import Info from "./Info";

const Navi = (props) => {
  return (
    <div className="navi">
      <Logo />
      <AddNew
        load={props.load}
        setLoad={props.setLoad}
        search={props.search}
        setSearch={props.setSearch}
        filial={props.filial}
        setFilial={props.setFilial}
      />
      <Info />
    </div>
  );
};

export default Navi;
