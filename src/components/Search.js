import React from 'react'

const Search = () => {
    return ( 
        <div className="search mt-5">

            <div className="form-group row ml-0 mr-0">
                <label  className="col-sm-2 col-form-label">Müştəri A.S.A</label>
                <div className="col-sm-4 d-flex">
                    <input type="email" className="form-control"  />
                    <button className="btn btn-info"><i className="fas fa-search"></i></button>
                    <button className="btn btn-primary"><i className="fas fa-chevron-down"></i></button>
                </div>
            </div>
            <div className="form-group row ml-0 mr-0">
                <label  className="col-sm-2 col-form-label">Search (keyWord)</label>
                <div className="col-sm-4 d-flex">
                    <input type="email" className="form-control" />
                    
                </div>
            </div>
            <div className="form-group row ml-0 mr-0">
                <label  className="col-sm-2 col-form-label">Folder</label>
                <div className="col-sm-4 d-flex">
                    <input type="email" className="form-control"  />

                    <button className="btn btn-danger"><i className="fas fa-plus"></i></button>
                    <button className="btn btn-info"><i className="fas fa-search"></i></button>
                    <button className="btn btn-primary"><i className="fas fa-chevron-down"></i></button>
                </div>
            </div>
            <div className="form-group row ml-0 mr-0">
                <label  className="col-sm-2 col-form-label">Status</label>
                <div className="col-sm-4 d-flex">
                    <input type="email" className="form-control"  />
                    <button className="btn btn-primary"><i className="fas fa-chevron-down"></i></button>
                </div>
            </div>
        </div>
     );
}
 
export default Search;