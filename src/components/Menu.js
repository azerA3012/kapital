import React, { useState } from "react";

const Menu = (props) => {
  const [menu, setMenu] = useState(false);

  const viewSearch=()=>{
    props.setSearch(true)
    props.setLoad(false)
    props.setFilial(false)
  }
  const viewFilial=()=>{
    props.setSearch(false)
    props.setLoad(false)
    props.setFilial(true)
  }
  return (
    <div className="menu">
      <ul className="list">
        <li className="list-item">
          <i className="fas fa-book"></i>
          <a href="#" className="list-link">
            Archive
          </a>
        </li>
        <li className="list-item">
          <i className="far fa-calendar-alt"></i>
          <a href="#" onClick={() => setMenu(!menu)} className="list-link">
            Main
          </a>
          {menu ? (
            <ul className="sub-menu">
              <li className="sub-menu-item">
                <a href="#" className="item-link">
                  bas ofis
                </a>
              </li>
              <li className="sub-menu-item">
                <a href="#" className="item-link" onClick={()=>viewFilial()}>
                  filial
                </a>
              </li>
            </ul>
          ) : null}
        </li>
        <li className="list-item">
          <i className="fas fa-search"></i>
          <a href="#" className="list-link" onClick={()=>viewSearch()}>
            Document search
            
          </a>
        </li>
        <li className="list-item">
          <i className="fas fa-th-list"></i>
          <a href="#" className="list-link">
            Create
          </a>
        </li>
        <li className="list-item">
          <i className="fas fa-flag"></i>
          <a href="#" className="list-link">
            Report
          </a>
        </li>
      </ul>
    </div>
  );
};

export default Menu;
