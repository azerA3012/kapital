import React from 'react'

const Info = () => {
    return ( 
        <div className="info">
            <div className="circle"></div>
            <div className="user-info">
                <div className="name">
                    Rahimli.E.A
                </div>
                <div className="icons d-flex">
                    <div className="message mr-2">
                        <i className="far fa-envelope"></i>
                    </div>
                    <div className="chevron">
                        <i className="fas fa-chevron-down"></i>
                    </div>
                    
                </div>
            </div>
        </div>
     );
}
 
export default Info;