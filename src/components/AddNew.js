import React from 'react'
 const AddNew = (props) => {
     const viewLoad=()=>{
         props.setLoad(true)
         props.setSearch(false);
         props.setFilial(false)
     }
     return ( 
         <div className="add-new">

             <button className="create-document" onClick={()=>viewLoad()}>
                    <span className="plus"><i className="fas fa-plus"></i></span>
                    <span>new document</span>
             </button>
             
         </div>
      );
 }
  
 export default AddNew;